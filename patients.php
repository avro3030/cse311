<?php require_once 'includes/top.php' ?>
    <title>Pharmacy Management : Patients </title>
<?php require_once 'includes/middle.php' ?>


    <div class="row">
        <div class="col-sm-6">
            <h2> Add new Patient</h2>
            <div class="small">

                <div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Patient Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter Patients Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Patient Address:</label>
                            <input type="text" class="form-control" id="address" placeholder="Enter Address"
                                   name="address">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Patient Phone:</label>
                            <input type="text" class="form-control" id="phone" placeholder="Enter Phone"
                                   name="phone">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Patient Email:</label>
                            <input type="text" class="form-control" id="email" placeholder="Enter Email"
                                   name="email">
                        </div>
                        <button type="submit" class="btn btn-default btn-add">Add</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-6">
            <h2> Edit a Patient</h2>
            <div class="small">
                <div class="edit-data">
                    <p class="alert">Click Edit Button to edit a doctor!</p>
                </div>

            </div>
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-sm">
            <h2> List of Patients</h2>
            <div class="small">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Patient ID</th>
                        <th>Patient Name</th>
                        <th>Address</th>
                        <th>Phone </th>
                        <th>Email </th>
                        <th>Edit/ Delete</th>
                    </tr>
                    </thead>
                    <tbody class="patients-data">

                    </tbody>
                </table>


            </div>
        </div>

    </div>


<?php require_once 'includes/bottom.php' ?>