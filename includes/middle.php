<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/custom.css">bit
</head>
<body class="wrapper">

<div class="container-fluid">
    <div class="row">
        <div class="col-sm">
            <div class="header">
                <h1 class="logo"> Pharmacy Management</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <?php require('menu.php'); ?>
            </nav>
        </div>
    </div>