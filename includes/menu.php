<a class="navbar-brand" href="/cse311">CSE311</a>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="/cse311/"> Home </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cse311/doctors.php">Doctors </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cse311/patients.php">Patients</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cse311/visits.php">Visits</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cse311/medicines.php">Medicines</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cse311/manufacturers.php">Manufacturers</a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
</div>