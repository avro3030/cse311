function getVisitData() {
    fetch('server.php/api/visits-detailed')
        .then((response) => response.json())
        .then((j) => {

            let str = [];

            for (let i = 0; i < j.length; i++) {
                str.push(`
               <tr><td> <b> ${j[i].visits_id} </b> </td>
               <td> ${j[i].patient_id} </td>
               <td> <b> ${j[i].patient_name} </b> </br> Phone: ${j[i].patient_phone} </br> Email: ${j[i].patient_email}  </br> Address: ${j[i].patient_address} </td>
               <td> ${j[i].doc_id} </td>
               <td> <b> ${j[i].doc_name} </b> (${j[i].doc_speciality}) </td>
               <td> ${j[i].visit_date} </td> 
              <td> <button onclick="editDataVisits(${j[i].visits_id})" class="btn-info btn-edit"> Edit </button> <button onclick="deleteData(${j[i].visits_id},'visits')" class="btn-danger btn-delete">Delete</button> </td> </tr>

               `);
            }

            $('.visits-data').html(str.join());
        });
}



function getMedicineData() {
    fetch('server.php/api/medicines')
        .then((response) => response.json())
        .then((j) => {

            let str = [];

            for (let i = 0; i < j.length; i++) {
                str.push(`
               <tr><td> <b> ${j[i].med_id} </b> </td>
               <td> ${j[i].Med_Name} </td>
               <td> ${j[i].Stock}  </td>
               <td> ${j[i].Manufacturer} </td>
       
              <td> <button onclick="editDataVisits(${j[i].med_id})" class="btn-info btn-edit"> Edit </button> <button onclick="deleteData(${j[i].med_id},'medicines')" class="btn-danger btn-delete">Delete</button> </td> </tr>

               `);
            }

            $('.medicines-data').html(str.join());
        });

}


function editDataVisits(id) {

    let url = `server.php/api/visits/${id}`;

    fetch(url)
        .then((response) => response.json())
        .then((j) => {

            let visits_id = j[0].visits_id;
            let Patient_id = j[0].Patient_id;
            let Doc_id = j[0].Doc_id;
            let visit_date = j[0].visit_date;

            let str = `<div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Patient ID:</label>
                            <input type="text" class="form-control" id="id-edit" value="${Patient_id}" placeholder="Enter Patient ID" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Doctors ID:</label>
                            <input type="text" class="form-control" id="doc-edit"value="${Doc_id}"  placeholder="Enter Doctors ID"
                                   name="speciality">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Visits Date:</label>
                            <input type="text" class="form-control" id="visit-date-edit" value="${visit_date}"  placeholder="Enter Visit Date"
                                   name="speciality">
                        </div>
                        <button onclick="updateDataVisits(${visits_id},'visits')" type="submit" class="btn btn-default btn-add">Update</button>
                    </div>
                </div>`;

            $('.edit-data').html(str);
        });
}

function updateDataVisits(id, str) {

    let data = {
        Patient_id: document.getElementById('id-edit').value,
        Doc_id: document.getElementById('doc-edit').value,
        visit_date: document.getElementById('visit-date-edit').value
    };

    let url = `server.php/api/${str}/update/${id}`;


    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "PUT",
        "headers": {
            "content-type": "application/json",
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {
        $('.edit-data').html(`<p style="padding: 10px" class="alert-success">Edited Successfully!</p>`);
        getVisitData()
    });

}






// Homepage

if (location.pathname === "/cse311/" || location.pathname === "/cse311" || location.pathname === "/cse311/index.php") {
    fetch('server.php/api/sells-detailed')
        .then((response) => response.json())
        .then((j) => {

            let str = [];

            for (let i = 0; i < 10; i++) {
                str.push(`
               <tr><td> <b> ${j[i].id} </b> </td>
               <td> ${j[i].pharm_id} </td>
               <td> <b> ${j[i].Name} </b> </br> Phone: ${j[i].Phone} </br> Address: ${j[i].Address} </td>
               <td> ${j[i].med_id} </td>
               <td> <b> ${j[i].Med_Name} </b> (${j[i].Manufacturer}) </td>
               <td> ${j[i].sells_date} </td> </tr>

               `);
            }

            $('.sells-data').append(str.join());
        });

    visitDetails();
}


// Doctors page

if (location.pathname === "/cse311/doctors.php") {

    getDoctors();

    $('.btn-add').click(() => {
        let data = {
            doc_name: document.getElementById('name').value,
            doc_speciality: document.getElementById('speciality').value
        };


        let settings = {
            "async": true,
            "crossDomain": true,
            "url": "server.php/api/doctors/add",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
            },
            "processData": false,
            "data": JSON.stringify(data)
        }

        $.ajax(settings).done(function (response) {
            getDoctors();
        });

    });
}

// Creating medicine specific page

if (location.pathname === "/cse311/medicines.php") {

    getMedicineData();

}


// Patients page

if (location.pathname === "/cse311/patients.php") {

    getPatients();

    $('.btn-add').click(() => {
        let data = {
            patient_name: document.getElementById('name').value,
            patient_address: document.getElementById('address').value,
            patient_phone: document.getElementById('phone').value,
            patient_email: document.getElementById('email').value
        };


        let settings = {
            "async": true,
            "crossDomain": true,
            "url": "server.php/api/patients/add",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
            },
            "processData": false,
            "data": JSON.stringify(data)
        }

        $.ajax(settings).done(function (response) {
            getPatients();
        });

    });
}


// Visits page

if (location.pathname === "/cse311/visits.php") {

    getVisitData();

    $('.btn-add').click(() => {
        let data = {
            Patient_id: document.getElementById('id').value,
            Doc_id: document.getElementById('doc').value,
            visit_date: document.getElementById('visit-date').value
        };


        let settings = {
            "async": true,
            "crossDomain": true,
            "url": "server.php/api/visits/add",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
            },
            "processData": false,
            "data": JSON.stringify(data)
        }

        $.ajax(settings).done(function (response) {
            getVisitData();
        });

    });
}

// All functions

function getDoctors() {
    fetch('server.php/api/doctors')
        .then((response) => response.json())
        .then((j) => {

            let str = [];

            for (let i = 0; i < j.length; i++) {
                str.push(`
               <tr><td> <b> ${j[i].doc_id} </b> </td>
               <td> <b> ${j[i].doc_name} </b></td>
               <td> ${j[i].doc_speciality} </td> 
               <td> <button onclick="editData(${j[i].doc_id})" class="btn-info btn-edit"> Edit </button> <button onclick="deleteData(${j[i].doc_id},'doctors')" class="btn-danger btn-delete">Delete</button> </td> </tr>

               `);
            }

            $('.doctors-data').html(str.join());
        });
}

function getPatients() {
    fetch('server.php/api/patients')
        .then((response) => response.json())
        .then((j) => {

            let str = [];

            for (let i = 0; i < j.length; i++) {
                str.push(`
               <tr><td> <b> ${j[i].patient_id} </b> </td>
               <td> <b> ${j[i].patient_name} </b></td>
               <td> ${j[i].patient_address} </td> 
               <td> ${j[i].patient_phone} </td>
               <td> ${j[i].patient_email} </td>
               <td> <button onclick="editDataPatient(${j[i].patient_id})" class="btn-info btn-edit"> Edit </button> <button onclick="deleteData(${j[i].patient_id},'patients')" class="btn-danger btn-delete">Delete</button> </td> </tr>

               `);
            }

            $('.patients-data').html(str.join());
        });
}

function deleteData(id, str) {

    let url = `server.php/api/${str}/delete/${id}`;

    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "DELETE",
        "headers": {
            "content-type": "application/json",
        },
        "processData": false,

    };


    $.ajax(settings).done(function (response) {
        console.log(response);
    });

    if(str==='doctors'){
        getDoctors();
    }

    if(str === 'patients') {
        getPatients();
    }
    if(str==='visits'){
        getVisitData();
    }
}

function editData(id) {

    let url = `server.php/api/doctors/${id}`;

    fetch(url)
        .then((response) => response.json())
        .then((j) => {

            let doc_id = j[0].doc_id;
            let doc_name = j[0].doc_name;
            let doc_speciality = j[0].doc_speciality;

            let str = `<div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Doctors Name:</label>
                            <input type="text" class="form-control" id="name-edit" value="${doc_name}" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Doctors Speciality:</label>
                            <input type="text" class="form-control" id="speciality-edit" value="${doc_speciality}"
                                   name="speciality">
                        </div>
                        <button onclick="updateData(${doc_id},'doctors')" type="submit" class="btn btn-default btn-add">Update</button>
                    </div>
                </div>`;

            $('.edit-data').html(str);
        });


}

function editDataPatient(id) {

    let url = `server.php/api/patients/${id}`;

    fetch(url)
        .then((response) => response.json())
        .then((j) => {

            let patient_id = j[0].patient_id;
            let patient_name = j[0].patient_name;
            let patient_address = j[0].patient_address;
            let patient_phone = j[0].patient_phone;
            let patient_email = j[0].patient_email;

            let str = `<div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Patient Name:</label>
                            <input type="text" class="form-control" id="name-edit" value="${patient_name}" placeholder="Enter Patients Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Patient Address:</label>
                            <input type="text" class="form-control" id="address-edit" value="${patient_address}" placeholder="Enter Address"
                                   name="address">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Patient Phone:</label>
                            <input type="text" class="form-control" id="phone-edit" value="${patient_phone}" placeholder="Enter Phone"
                                   name="phone">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Patient Email:</label>
                            <input type="text" class="form-control" id="email-edit" value="${patient_email}" placeholder="Enter Email"
                                   name="email">
                        </div>
                        <button onclick="updateDataPatient(${patient_id},'patients')" type="submit" class="btn btn-default btn-add">Update</button>
                    </div>
                </div>`;

            $('.edit-data').html(str);
        });
}

function updateData(id, str) {

    let data = {
        doc_name: document.getElementById('name-edit').value,
        doc_speciality: document.getElementById('speciality-edit').value
    };

    let url = `server.php/api/${str}/update/${id}`;


    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "PUT",
        "headers": {
            "content-type": "application/json",
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {
        $('.edit-data').html(`<p style="padding: 10px" class="alert-success">Edited Successfully!</p>`);
        getDoctors();
    });

}

function updateDataPatient(id, str) {

    let data = {
        patient_name: document.getElementById('name-edit').value,
        patient_address: document.getElementById('address-edit').value,
        patient_phone: document.getElementById('phone-edit').value,
        patient_email: document.getElementById('email-edit').value
    };

    let url = `server.php/api/${str}/update/${id}`;


    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "PUT",
        "headers": {
            "content-type": "application/json",
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {
        $('.edit-data').html(`<p style="padding: 10px" class="alert-success">Edited Successfully!</p>`);
        getPatients();
    });

}

function visitDetails() {
    fetch('server.php/api/visits-detailed')
        .then((response) => response.json())
        .then((j) => {

            let str = [];

            for (let i = 0; i < 10; i++) {
                str.push(`
               <tr><td> <b> ${j[i].visits_id} </b> </td>
               <td> ${j[i].patient_id} </td>
               <td> <b> ${j[i].patient_name} </b> </br> Phone: ${j[i].patient_phone} </br> Email: ${j[i].patient_email}  </br> Address: ${j[i].patient_address} </td>
               <td> ${j[i].doc_id} </td>
               <td> <b> ${j[i].doc_name} </b> (${j[i].doc_speciality}) </td>
               <td> ${j[i].visit_date} </td> </tr>

               `);
            }

            $('.visits-data').append(str.join());
        });
}







