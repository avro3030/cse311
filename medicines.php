<?php require_once 'includes/top.php' ?>
    <title>Pharmacy Management : Medicines </title>
<?php require_once 'includes/middle.php' ?>


    <div class="row">
        <div class="col-sm-6">
            <h2> Add new Doctor</h2>
            <div class="small">

                <div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Medicines Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter Medicines Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Medicines Speciality:</label>
                            <input type="text" class="form-control" id="speciality" placeholder="Enter Speciality"
                                   name="speciality">
                        </div>
                        <button type="submit" class="btn btn-default btn-add">Add</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-6">
            <h2> Edit a Doctor</h2>
            <div class="small">
                <div class="edit-data">
                    <p class="alert">Click Edit Button to edit a doctor!</p>
                </div>

            </div>
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-sm">
            <h2> List of Medicines</h2>
            <div class="small">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Med ID</th>
                        <th>Med Name</th>
                        <th>Stock</th>
                        <th>Manufacturer</th>
                        <th>Edit/ Delete</th>
                    </tr>
                    </thead>
                    <tbody class="medicines-data">

                    </tbody>
                </table>


            </div>
        </div>

    </div>


<?php require_once 'includes/bottom.php' ?>


