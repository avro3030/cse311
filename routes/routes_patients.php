<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// All Patients
$app->get('/api/patients', function (Request $request, Response $response) {
    $sql = "SELECT * FROM patient ORDER BY patient_id DESC";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sells = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sells);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// Get One single sell

$app->get('/api/patients/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM patient WHERE patient_id = $id LIMIT 1";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sell = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sell);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Add one sell

$app->post('/api/patients/add', function (Request $request, Response $response) {

    // Get all the fields
    $patient_name = $request->getParam('patient_name');
    $patient_address = $request->getParam('patient_address');
    $patient_phone = $request->getParam('patient_phone');
    $patient_email = $request->getParam('patient_email');

    $sql = "INSERT INTO patient (patient_name, patient_address, patient_phone,patient_email) VALUES (:patient_name,:patient_address,:patient_phone,:patient_email)";


    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':patient_name', $patient_name);
        $stmt->bindParam(':patient_address', $patient_address);
        $stmt->bindParam(':patient_phone', $patient_phone);
        $stmt->bindParam(':patient_email', $patient_email);

        $stmt->execute();

        echo '{"notice": {"text":"data has been added"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Update patient

$app->put('/api/patients/update/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');
    $patient_name = $request->getParam('patient_name');
    $patient_address = $request->getParam('patient_address');
    $patient_phone = $request->getParam('patient_phone');
    $patient_email = $request->getParam('patient_email');

    $sql = "UPDATE patient SET patient_name = COALESCE(:patient_name,patient_name), patient_address = COALESCE(:patient_address,patient_address), 
            patient_phone = COALESCE(:patient_phone,patient_phone), 
            patient_email = COALESCE(:patient_email,patient_email) WHERE patient_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':patient_name', $patient_name);
        $stmt->bindParam(':patient_address', $patient_address);
        $stmt->bindParam(':patient_phone', $patient_phone);
        $stmt->bindParam(':patient_email', $patient_email);

        $stmt->execute();

        echo '{"notice": {"text":"data has been updated"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Delete sell


$app->delete('/api/patients/delete/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');


    $sql = "DELETE FROM patient WHERE patient_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->execute();

        echo '{"notice": {"text":"data has been deleted"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

