<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// All Sells
$app->get('/api/sells', function (Request $request, Response $response) {
    $sql = "SELECT * FROM sells ORDER BY id DESC";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sells = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sells);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// All Sells joined with pharmacy and medicine
$app->get('/api/sells-detailed', function (Request $request, Response $response) {
    $sql = "SELECT sells.id,sells.pharm_id,pharmacy.Name,pharmacy.Phone,pharmacy.Address, sells.med_id,medicine.Med_Name,medicine.Manufacturer, sells.sells_date
            FROM sells, medicine, pharmacy
            WHERE sells.pharm_id = pharmacy.pharm_id AND sells.med_id = medicine.med_id
            ORDER BY sells.id DESC";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sells = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sells);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// Get One single sell

$app->get('/api/sells/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM sells WHERE pharm_id = $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sell = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sell);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Add one sell

$app->post('/api/sells/add', function (Request $request, Response $response) {

    // Get all the fields
    $pharm_id = $request->getParam('pharm_id');
    $med_id = $request->getParam('med_id');
    $sells_date = $request->getParam('sells_date');

    $sql = "INSERT INTO sells (pharm_id, med_id, sells_date) VALUES (:pharm_id,:med_id,:sells_date)";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':pharm_id', $pharm_id);
        $stmt->bindParam(':med_id', $med_id);
        $stmt->bindParam(':sells_date', $sells_date);

        $stmt->execute();

        echo '{"notice": {"text":"Sell data has been added"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Update sell

$app->put('/api/sells/update/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');
    $pharm_id = $request->getParam('pharm_id');
    $med_id = $request->getParam('med_id');
    $sells_date = $request->getParam('sells_date');

    $sql = "UPDATE sells SET pharm_id = COALESCE(:pharm_id,pharm_id), med_id = COALESCE(:med_id,med_id), sells_date = COALESCE(:sells_date,sells_date) WHERE pharm_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':pharm_id', $pharm_id);
        $stmt->bindParam(':med_id', $med_id);
        $stmt->bindParam(':sells_date', $sells_date);

        $stmt->execute();

        echo '{"notice": {"text":"Sell data has been updated"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Delete sell


$app->delete('/api/sells/delete/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');


    $sql = "DELETE FROM sells WHERE pharm_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->execute();

        echo '{"notice": {"text":"Sell data has been deleted"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

