<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// All Sells
$app->get('/api/doctors', function (Request $request, Response $response) {
    $sql = "SELECT * FROM doctor ORDER BY doc_id DESC";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sells = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sells);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// Get One single sell

$app->get('/api/doctors/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM doctor WHERE doc_id = $id LIMIT 1";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sell = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sell);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Add one sell

$app->post('/api/doctors/add', function (Request $request, Response $response) {

    // Get all the fields
    $doc_name = $request->getParam('doc_name');
    $doc_speciality = $request->getParam('doc_speciality');

    $sql = "INSERT INTO doctor (doc_name, doc_speciality) VALUES (:doc_name,:doc_speciality)";


    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':doc_name', $doc_name);
        $stmt->bindParam(':doc_speciality', $doc_speciality);

        $stmt->execute();

        echo '{"notice": {"text":"data has been added"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Update sell

$app->put('/api/doctors/update/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');
    $doc_name = $request->getParam('doc_name');
    $doc_speciality = $request->getParam('doc_speciality');

    $sql = "UPDATE doctor SET doc_name = COALESCE(:doc_name,doc_name), doc_speciality = COALESCE(:doc_speciality,doc_speciality) WHERE doc_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':doc_name', $doc_name);
        $stmt->bindParam(':doc_speciality', $doc_speciality);

        $stmt->execute();

        echo '{"notice": {"text":"data has been updated"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Delete sell


$app->delete('/api/doctors/delete/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');


    $sql = "DELETE FROM doctor WHERE doc_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->execute();

        echo '{"notice": {"text":"data has been deleted"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

