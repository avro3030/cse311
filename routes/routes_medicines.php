<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// All medicines
$app->get('/api/medicines', function (Request $request, Response $response) {
    $sql = "SELECT * FROM medicine ORDER BY med_id DESC";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sells = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sells);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// Get One single sell

$app->get('/api/medicines/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM medicine WHERE med_id = $id LIMIT 1";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sell = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sell);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Add one sell

$app->post('/api/medicines/add', function (Request $request, Response $response) {

    // Get all the fields
    $Med_Name = $request->getParam('Med_Name');
    $Stock = $request->getParam('Stock');
    $Manufacturer = $request->getParam('Manufacturer');

    $sql = "INSERT INTO medicine (Med_Name, Stock, Manufacturer) VALUES (:Med_Name,:Stock,:Manufacturer)";


    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':Med_Name', $Med_Name);
        $stmt->bindParam(':Stock', $Stock);
        $stmt->bindParam(':Manufacturer', $Manufacturer);

        $stmt->execute();

        echo '{"notice": {"text":"data has been added"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Update patient

$app->put('/api/medicines/update/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');
    $Med_Name = $request->getParam('Med_Name');
    $Stock = $request->getParam('Stock');
    $Manufacturer = $request->getParam('Manufacturer');

    $sql = "UPDATE medicine SET Med_Name = COALESCE(:Med_Name,Med_Name), Stock = COALESCE(:Stock,Stock), 
            Manufacturer = COALESCE(:Manufacturer,Manufacturer) 
            WHERE med_id = $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':Med_Name', $Med_Name);
        $stmt->bindParam(':Stock', $Stock);
        $stmt->bindParam(':Manufacturer', $Manufacturer);

        $stmt->execute();

        echo '{"notice": {"text":"data has been updated"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Delete sell


$app->delete('/api/medicines/delete/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');


    $sql = "DELETE FROM medicine WHERE med_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->execute();

        echo '{"notice": {"text":"data has been deleted"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

