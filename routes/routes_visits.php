<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// All visits
$app->get('/api/visits', function (Request $request, Response $response) {
    $sql = "SELECT * FROM visits ORDER BY visits_id DESC";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $visits = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($visits);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// All visits joined with patient and doctor
$app->get('/api/visits-detailed', function (Request $request, Response $response) {
    $sql = "SELECT visits.visits_id, visits.visit_date, patient.patient_id, patient.patient_name, patient.patient_email, patient.patient_phone, patient.patient_address, doctor.doc_id, doctor.doc_name, doctor.doc_speciality
            FROM visits, patient, doctor
            WHERE visits.Doc_id = doctor.doc_id AND visits.Patient_id = patient.patient_id
            ORDER BY visits.visits_id DESC";


    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sells = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sells);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});


// Get One single visit

$app->get('/api/visits/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM visits WHERE visits_id = $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sell = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($sell);

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Add one sell

$app->post('/api/visits/add', function (Request $request, Response $response) {

    // Get all the fields
    $Patient_id = $request->getParam('Patient_id');
    $Doc_id = $request->getParam('Doc_id');
    $visit_date = $request->getParam('visit_date');

    $sql = "INSERT INTO visits (Patient_id,Doc_id, visit_date) VALUES (:Patient_id,:Doc_id,:visit_date)";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':Patient_id', $Patient_id);
        $stmt->bindParam(':Doc_id', $Doc_id);
        $stmt->bindParam(':visit_date', $visit_date);

        $stmt->execute();

        echo '{"notice": {"text":"Sell data has been added"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Update sell

$app->put('/api/visits/update/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');
    $Patient_id = $request->getParam('Patient_id');
    $Doc_id = $request->getParam('Doc_id');
    $visit_date = $request->getParam('visit_date');

    $sql = "UPDATE visits SET Patient_id = COALESCE(:Patient_id,Patient_id), Doc_id = COALESCE(:Doc_id,Doc_id), visit_date = COALESCE(:visit_date,visit_date) WHERE visits_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':Patient_id', $Patient_id);
        $stmt->bindParam(':Doc_id', $Doc_id);
        $stmt->bindParam(':visit_date', $visit_date);

        $stmt->execute();

        echo '{"notice": {"text":"Sell data has been updated"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});

// Delete sell


$app->delete('/api/visits/delete/{id}', function (Request $request, Response $response) {

    // Get all the fields
    $id = $request->getAttribute('id');


    $sql = "DELETE FROM visits WHERE visits_id= $id";

    try {

        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->execute();

        echo '{"notice": {"text":"Sell data has been deleted"}';

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
    }

});