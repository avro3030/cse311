<?php require_once 'includes/top.php' ?>
    <title>Pharmacy Management : Doctors </title>
<?php require_once 'includes/middle.php' ?>


    <div class="row">
        <div class="col-sm-6">
            <h2> Add new Doctor</h2>
            <div class="small">

                <div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Doctors Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter Doctors Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Doctors Speciality:</label>
                            <input type="text" class="form-control" id="speciality" placeholder="Enter Speciality"
                                   name="speciality">
                        </div>
                        <button type="submit" class="btn btn-default btn-add">Add</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-6">
            <h2> Edit a Doctor</h2>
            <div class="small">
                <div class="edit-data">
                    <p class="alert">Click Edit Button to edit a doctor!</p>
                </div>

            </div>
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-sm">
            <h2> List of Doctors</h2>
            <div class="small">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Doc ID</th>
                        <th>Doctors Name</th>
                        <th>Speciality</th>
                        <th>Edit/ Delete</th>
                    </tr>
                    </thead>
                    <tbody class="doctors-data">

                    </tbody>
                </table>


            </div>
        </div>

    </div>


<?php require_once 'includes/bottom.php' ?>