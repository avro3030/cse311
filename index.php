<?php require_once 'includes/top.php' ?>
    <title>Pharmacy Management : NSU CSE 311</title>
<?php require_once 'includes/middle.php' ?>
    <div class="row">
        <div class="col-sm">
            <h2> Recent 10 Medicine Sells</h2>
            <div class="small">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Sell ID</th>
                        <th>Pharm ID</th>
                        <th>Pharm Name</th>
                        <th>Med ID</th>
                        <th>Med Name</th>
                        <th>Sale Date</th>
                    </tr>
                    </thead>
                    <tbody class="sells-data">

                    </tbody>
                </table>


            </div>
        </div>

    </div>


    <hr>


    <div class="row">
        <div class="col-sm">
            <h2> Recent 10 Doctors Visits</h2>
            <div class="small">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Visit ID</th>
                        <th>Patient ID</th>
                        <th>Patient Details</th>
                        <th>Doc ID</th>
                        <th>Doc Details</th>
                        <th>Visit Date</th>
                    </tr>
                    </thead>
                    <tbody class="visits-data">

                    </tbody>
                </table>


            </div>
        </div>

    </div>
<?php require_once 'includes/bottom.php' ?>