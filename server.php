<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require './vendor/autoload.php';
require './config/db.php';



$app = new \Slim\App;


// Pharmacy routes
require './routes/routes_sells.php';

//Homepage routes
require './routes/routes_home.php';

//Visits routes
require './routes/routes_visits.php';

//Doctors routes
require './routes/routes_doctors.php';

//Patients routes
require './routes/routes_patients.php';

//Medicines routes
require './routes/routes_medicines.php';


$app->run();
