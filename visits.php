<?php require_once 'includes/top.php' ?>
    <title>Pharmacy Management : Visits </title>
<?php require_once 'includes/middle.php' ?>


    <div class="row">
        <div class="col-sm-6">
            <h2> Add new Visit</h2>
            <div class="small">

                <div class="container">
                    <div>
                        <div class="form-group">
                            <label for="name">Patient ID:</label>
                            <input type="text" class="form-control" id="id" placeholder="Enter Patient ID" name="name">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Doctors ID:</label>
                            <input type="text" class="form-control" id="doc" placeholder="Enter Doctors ID"
                                   name="speciality">
                        </div>
                        <div class="form-group">
                            <label for="speciality">Visits Date:</label>
                            <input type="text" class="form-control" id="visit-date" placeholder="Enter Visit Date"
                                   name="speciality">
                        </div>
                        <button type="submit" class="btn btn-default btn-add">Add</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-6">
            <h2> Edit a Visit</h2>
            <div class="small">
                <div class="edit-data">
                    <p class="alert">Click Edit Button to edit a Visit!</p>
                </div>

            </div>
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-sm">
            <h2> List of Doctors Visits</h2>
            <div class="small">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Visit ID</th>
                        <th>Patient ID</th>
                        <th>Patient Details</th>
                        <th>Doc ID</th>
                        <th>Doc Details</th>
                        <th>Visit Date</th>
                        <th>Edit/Delete</th>
                    </tr>
                    </thead>
                    <tbody class="visits-data">

                    </tbody>
                </table>


            </div>
        </div>

    </div>


<?php require_once 'includes/bottom.php' ?>