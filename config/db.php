<?php

class db
{
    private $servername = "52.205.249.230";
    private $username = "cse311";
    private $password = "NsuCse";
    private $dbname = "pharmacy";

    public function connect(){
        $mysql_connect_str = "mysql:host=$this->servername;dbname=$this->dbname";
        $dbConnection = new PDO($mysql_connect_str,$this->username,$this->password);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $dbConnection;
    }
}